import pychromecast
import feedparser


# Set variables
hostname = "192.168.177.4" # ip of roku
rss = 'https://www.npr.org/rss/podcast.php?id=510318' # rss of podcast NPR up first

feed = feedparser.parse(rss)

url = feed.entries[0].links[0].get('href', '') # url pulled from feed, should be latest
type_u = feed.entries[0].links[0].get('type', '') # type pulled from feed
pic = feed.entries[0].image.get('href', '') # url of picture pulled from feed
tit = "wake up you piece of shit!" # a friendly reminder


# run playbook

# Your Chromecast device Friendly Name
device_friendly_name = "Alarm"
print("friendly name set")

chromecasts = pychromecast.get_chromecasts()
print("getting all cc")
# select Chromecast device
cast = next(cc for cc in chromecasts if cc.device.friendly_name == device_friendly_name)
print("set to alarm")
# wait for the device
cast.wait()
print("waiting")
print(cast.device)
print(cast.status)

# get media controller
mc = cast.media_controller
print("get mc")
# set online video url
mc.play_media(url, type_u, tit, pic)

print("setting to latest up first episode")
# blocks device
mc.block_until_active()
print(mc.status)

# plays the video
print("starting")
mc.play()


